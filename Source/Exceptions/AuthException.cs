﻿using System;

namespace Source.Exceptions
{
    public class AuthException : Exception
    {
        public bool UsernameIncorrect { get; set; }
        public bool PasswordIncorrect { get; set; }

        public AuthException(string message, bool usernameIncorrect, bool passwordIncorrect)
            : base(message)
        {
            UsernameIncorrect = usernameIncorrect;
            PasswordIncorrect = passwordIncorrect;
        }
    }
}