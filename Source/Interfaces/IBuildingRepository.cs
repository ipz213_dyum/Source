﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using Source.Models;

namespace Source.Interfaces
{
    public interface IBuildingRepository
    {
        List<Building> GetBuildings(int complexId);
        int GetBuildingsCount(int complexId);
        void AddBuilding(Building building);
        void EditBuilding(Building building);
        void DeleteBuilding(int buildingId);
    }
}