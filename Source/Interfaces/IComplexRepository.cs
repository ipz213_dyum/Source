﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using Source.Models;

namespace Source.Interfaces
{
    public interface IComplexRepository
    {
        List<Complex> GetComplexes();
        int GetComplexesCount();
        List<Inhabitant> GetComplexInhabitants(int complexId);
        int GetComplexInhabitantsCount(int complexId);
        int GetBuildingsCount(int complexId);
        Complex GetComplex(int complexId);
        void AddComplex(Complex complex);
        void EditComplex(Complex complex);
        void DeleteComplex(int complexId);
    }
}