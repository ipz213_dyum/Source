﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using Source.Models;

namespace Source.Interfaces
{
    public interface IInhabitantRepository
    {
        List<Inhabitant> GetBuildingInhabitants(int buildingId);
        int GetBuildingInhabitantsCount(int buildingId);
        void AddInhabitant(Inhabitant inhabitant);
        void EditInhabitant(Inhabitant inhabitant);
        void DeleteInhabitant(int inhabitantId);
    }
}