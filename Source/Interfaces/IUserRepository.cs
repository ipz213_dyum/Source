﻿using System.Collections.ObjectModel;
using Source.Models;

namespace Source.Interfaces
{
    public interface IUserRepository
    {
        User Login(string login, string password);
        User Register(string login, string password);
    }
}