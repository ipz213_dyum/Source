﻿namespace Source.Interfaces
{
    public interface IView
    {
        bool? Open();
        void initializeOnClosing();
    }
}