﻿using Source.Repositories;

namespace Source.Models
{
    public class Building
    {
        private InhabitantRepository inhabitantRepository = new InhabitantRepository();

        public int Id { get; }
        public string Name { get; set; }
        public int ComplexId { get; }
        public string Address { get; set; }

        public int InhabitantsCount
        {
            get
            {
                return  inhabitantRepository.GetBuildingInhabitantsCount(Id);
            }
        }


        public Building(int id, string name, int complexId, string address)
        {
            Id = id;
            Name = name;
            ComplexId = complexId;
            Address = address;
        }
        public Building(string name, int complexId, string address)
        {
            Name = name;
            ComplexId = complexId;
            Address = address;
        }
    }
}