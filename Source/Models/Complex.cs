﻿using System;
using System.Threading;
using Source.Repositories;

namespace Source.Models
{
    public class Complex
    {
        public int Id { get; }
        public int OwnerId{get;}
        public string City { get; set; }
        public string Name { get; set; }
        public int BuildingsCount { get; set; }
        public int InhabitantsCount { get; set; }
        private ComplexRepository ComplexRepository = new ComplexRepository();

        public Complex(int id, int ownerId, string city, string name)
        {
            Id = id;
            OwnerId = ownerId;
            City = city;
            Name = name;
            BuildingsCount = ComplexRepository.GetBuildingsCount(id);
            InhabitantsCount = ComplexRepository.GetComplexInhabitantsCount(id);
        }
        public Complex(string city, string name)
        {
            City = city;
            Name = name;
        }
    }
}