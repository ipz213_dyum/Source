﻿using System;
using System.Windows.Documents;
using Source.Annotations;

namespace Source.Models
{
    public class Inhabitant
    {
        public enum Sex
        {
            NotKnown,
            Male,
            Female,
            Other,
        }

        public int Id { get; }
        public int BuildingId { get; set; }
        public string Flat { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string MiddleName { get; set; }
        public Sex WhichSex { get; set; }

        public string SexString
        {
            get
            {
                if (WhichSex == Sex.Male)
                {
                    return "Male";
                }

                if (WhichSex == Sex.Female)
                {
                    return "Female";
                }

                if (WhichSex == Sex.Other)
                {
                    return "Other";
                }

                return "Not known";
            }
        }
        public DateTime BirthDate { get; set; }
        public DateTime SettlementDate { get; set; }
        public DateTime? EvictionDate { get; set; }
        public string PassportId { get; set; }
        public string WorkPlace { get; set; }
        public string Position { get; set; }

        public int Age
        {
            get { return GetAge(); }
        }

        public string PhoneNumber { get; set; }

        public Inhabitant()
        {
        }

        public Inhabitant(int id, int buildingId, string flat, string firstName, string lastName, string middleName,
            Byte whichSex, DateTime birthDate, DateTime settlementDate, DateTime? evictionDate, string passportId,
            [CanBeNull] string workPlace, [CanBeNull] string position, string phoneNumber)
        {
            Id = id;
            BuildingId = buildingId;
            Flat = flat;
            FirstName = firstName;
            LastName = lastName;
            MiddleName = middleName;
            PhoneNumber = phoneNumber;
            WhichSex = (Sex)whichSex;
            BirthDate = birthDate;
            SettlementDate = settlementDate;
            EvictionDate = evictionDate;
            PassportId = passportId;
            WorkPlace = workPlace;
            Position = position;
        }

        public int GetAge()
        {
            DateTime today = DateTime.Now;
            int age = today.Year - BirthDate.Year;
            if (BirthDate.Date > today.AddYears(-age)) age--;
            return age;
        }

        public int GetResidenceTime()
        {
            DateTime today = DateTime.Now;
            TimeSpan residenceTime;
            if (EvictionDate == null)
            {
                residenceTime = today.Subtract(SettlementDate);
            }
            else
            {
                residenceTime = (TimeSpan)EvictionDate?.Subtract(SettlementDate);
            }

            return residenceTime.Days;
        }
    }
}