﻿using Source.Views;

namespace Source.Models
{
    public class User
    {
        public int Id { get; }
        public string Login { get; set; }

        public User()
        {
        }

        public User(int id, string login)
        {
            this.Id = id;
            this.Login = login;
        }
        
    }
}