﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using Dapper;
using Source.Interfaces;
using Source.Models;

namespace Source.Repositories
{
    public class BuildingRepository : IBuildingRepository
    {
        private static readonly string ConnectionString =@"Data Source=SLAP\SQLEXPRESS;Initial Catalog=Source;Integrated Security=True";

        public List<Building> GetBuildings(int complexId)
        {
            using (SqlConnection connection = new SqlConnection(ConnectionString))
            {
                List<Building> buildings = connection.Query<Building>(
                    @"SELECT * FROM Buildings WHERE ComplexId = @ComplexId",
                    new
                    {
                        ComplexId = complexId
                    }).ToList();
                return buildings;
            }
        }

        public int GetBuildingsCount(int complexId)
        {
            using (SqlConnection connection = new SqlConnection(ConnectionString))
            {
                int buildingsCount = connection.Query<int>(
                    @"SELECT COUNT(*) FROM Buildings WHERE ComplexId = @ComplexId",
                    new
                    {
                        ComplexId = complexId
                    }).FirstOrDefault();
                return buildingsCount;
            }
        }

        public void AddBuilding(Building building)
        {
            if (checkBuildValid(building))
            {
                using (SqlConnection connection = new SqlConnection(ConnectionString))
                {
                    connection.Query(@"INSERT INTO Buildings (Name, ComplexId, Address) 
                    VALUES (@Name, @ComplexId, @Address)",
                        new
                        {
                            Name = building.Name,
                            ComplexId = building.ComplexId,
                            Address = building.Address
                        });
                }
            }
        }

        public void EditBuilding(Building building)
        {
            if (checkBuildValid(building))
            {
                using (SqlConnection connection = new SqlConnection(ConnectionString))
                {
                    connection.Query(@"UPDATE Buildings
                    SET Name = @Name, ComplexId = @ComplexId, Address = @Address
                    WHERE Id = @Id",
                        new
                        {
                            Id = building.Id,
                            Name = building.Name,
                            ComplexId = building.ComplexId,
                            Address = building.Address
                        });
                }
            }
        }
        public void DeleteBuilding(int buildingId)
        {
            using (SqlConnection connection = new SqlConnection(ConnectionString))
            {
                connection.Query(@"DELETE FROM Buildings WHERE Id = @Id",
                    new
                    {
                        Id = buildingId
                    });
            }
        }

        public bool checkBuildValid(Building building)
        {
            if (building == null)
            {
                throw new Exception("Complete all fields");
            }

            if (building.Name == null || building.Address == null)
            {
                throw new Exception("Complete all fields");
            }

            if (building.Name.Trim().Equals("") || building.Address.Trim().Equals(""))
            {
                throw new Exception("Complete all fields");
            }

            return true;
        }
    }
}