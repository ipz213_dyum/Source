﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using Dapper;
using Source.Interfaces;
using Source.Models;

namespace Source.Repositories
{
    public class ComplexRepository : IComplexRepository
    {
        private static readonly string ConnectionString =@"Data Source=SLAP\SQLEXPRESS;Initial Catalog=Source;Integrated Security=True";

        public List<Complex> GetComplexes()
        {
            if (checkAuth())
            {
                using (SqlConnection connection = new SqlConnection(ConnectionString))
                {
                    List<Complex> complexes = connection.Query<Complex>(
                        @"SELECT * FROM Complexes Where OwnerId = @OwnerId",
                        new
                        {
                            OwnerId = UserRepository.CurrentUser.Id
                        }).ToList();
                    return complexes;
                }
            }

            return null;
        }

        public int GetComplexesCount()
        {
            if (checkAuth())
            {
                using (SqlConnection connection = new SqlConnection(ConnectionString))
                {
                    int count = connection.Query<int>(
                        @"SELECT COUNT(*) FROM Complexes Where OwnerId = @OwnerId",
                        new
                        {
                            OwnerId = UserRepository.CurrentUser.Id
                        }).FirstOrDefault();
                    return count;
                }
            }

            return 0;
        }

        public List<Inhabitant> GetComplexInhabitants(int complexId)
        {
            using (SqlConnection connection = new SqlConnection(ConnectionString))
            {
                List<Inhabitant> inhabitants = connection.Query<Inhabitant>(
                    @"SELECT * FROM Inhabitants WHERE BuildingId IN (SELECT Id FROM Buildings WHERE Buildings.ComplexId = @ComplexId) ORDER BY Id DESC",
                    new
                    {
                        ComplexId = complexId
                    }).ToList();
                return inhabitants;
            }
        }

        public int GetComplexInhabitantsCount(int complexId)
        {
            using (SqlConnection connection = new SqlConnection(ConnectionString))
            {
                int inhabitantsCount = connection.Query<int>(
                    @"SELECT COUNT(*) FROM Inhabitants 
                            WHERE BuildingId IN
	                            (
		                            SELECT Id FROM Buildings
		                            WHERE Buildings.ComplexId = @ComplexId
	                            );",
                    new
                    {
                        ComplexId = complexId
                    }).FirstOrDefault();
                return inhabitantsCount;
            }
        }

        public int GetBuildingsCount(int complexId)
        {
            using (SqlConnection connection = new SqlConnection(ConnectionString))
            {
                int buildingsCount = connection.Query<int>(
                    @"SELECT COUNT(*) FROM Buildings
                            WHERE ComplexId = @ComplexId",
                    new
                    {
                        ComplexId = complexId
                    }).FirstOrDefault();
                return buildingsCount;
            }
        }

        public Complex GetComplex(int complexId)
        {
            using (SqlConnection connection = new SqlConnection(ConnectionString))
            {
                Complex complex = connection.Query<Complex>(
                    @"SELECT * FROM Complexes
                            WHERE Id = @Id",
                    new
                    {
                        Id = complexId
                    }).FirstOrDefault();
                return complex;
            }
        }

        public void AddComplex(Complex complex)
        {
            if (checkAuth() && checkComplexValid(complex))
            {
                using (SqlConnection connection = new SqlConnection(ConnectionString))
                {
                    connection.Query(@"INSERT INTO Complexes (OwnerId, City, Name) 
                    VALUES (@OwnerId, @City, @Name)",
                        new
                        {
                            OwnerId = UserRepository.CurrentUser.Id,
                            City = complex.City,
                            Name = complex.Name,
                        });
                }
            }
        }

        public void EditComplex(Complex complex)
        {
            using (SqlConnection connection = new SqlConnection(ConnectionString))
            {
                connection.Query(@"UPDATE Complexes 
                        SET City = @City, Name = @Name
                        WHERE Id = @Id",
                    new
                    {
                        Id = complex.Id,
                        City = complex.City,
                        Name = complex.Name,
                    });
            }
        }

        public void DeleteComplex(int complexId)
        {
            using (SqlConnection connection = new SqlConnection(ConnectionString))
            {
                connection.Query(@"DELETE FROM Complexes WHERE Id = @Id",
                    new
                    {
                        Id = complexId
                    });
            }
        }

        public bool checkAuth()
        {
            if (!UserRepository.ifUserAuthorized())
            {
                throw new Exception("You need to log in");
            }

            return true;
        }

        public bool checkComplexValid(Complex complex)
        {
            if (complex == null)
            {
                throw new Exception("Complete all fields");
            }
            if (complex.Name == null || complex.City == null)
            {
                throw new Exception("Complete all fields");
            }
            if (complex.Name.Trim().Equals("") || complex.City.Trim().Equals(""))
            {
                throw new Exception("Complete all fields");
            }
            return true;
        }
    }
}