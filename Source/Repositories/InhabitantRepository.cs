﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using Dapper;
using Source.Annotations;
using Source.Interfaces;
using Source.Models;

namespace Source.Repositories
{
    public class InhabitantRepository : IInhabitantRepository
    {
        private static readonly string ConnectionString =@"Data Source=SLAP\SQLEXPRESS;Initial Catalog=Source;Integrated Security=True";

        public List<Inhabitant> GetBuildingInhabitants(int buildingId)
        {
            using (SqlConnection connection = new SqlConnection(ConnectionString))
            {
                List<Inhabitant> inhabitants = connection.Query<Inhabitant>(
                    @"SELECT * FROM Inhabitants WHERE BuildingId = @BuildingId ORDER BY Id DESC",
                    new
                    {
                        BuildingId = buildingId
                    }).ToList();
                return inhabitants;
            }
        }

        public int GetBuildingInhabitantsCount(int buildingId)
        {
            using (SqlConnection connection = new SqlConnection(ConnectionString))
            {
                int inhabitantsCount = connection.Query<int>(
                    @"SELECT COUNT(*) FROM Inhabitants WHERE BuildingId = @BuildingId",
                    new
                    {
                        BuildingId = buildingId
                    }).FirstOrDefault();
                return inhabitantsCount;
            }
        }

        public void AddInhabitant(Inhabitant inhabitant)
        {
            if (checkInhabitantValid(inhabitant))
            {
                using (SqlConnection connection = new SqlConnection(ConnectionString))
                {
                    connection.Query(
                        @"INSERT INTO Inhabitants (BuildingId, Flat, FirstName, LastName, MiddleName, WhichSex, BirthDate, 
                         SettlementDate,EvictionDate, PassportId, WorkPlace, Position, PhoneNumber)
                        VALUES (@BuildingId, @Flat, @FirstName, @LastName, @MiddleName, @WhichSex, @BirthDate, 
                                @SettlementDate, @EvictionDate, @PassportId, @WorkPlace, @Position, @PhoneNumber)",
                        new
                        {
                            BuildingId = inhabitant.BuildingId,
                            Flat = inhabitant.Flat,
                            FirstName = inhabitant.FirstName,
                            LastName = inhabitant.LastName,
                            MiddleName = inhabitant.MiddleName,
                            WhichSex = inhabitant.WhichSex,
                            BirthDate = inhabitant.BirthDate,
                            SettlementDate = inhabitant.SettlementDate,
                            EvictionDate = inhabitant.EvictionDate,
                            PassportId = inhabitant.PassportId,
                            WorkPlace = inhabitant.WorkPlace,
                            Position = inhabitant.Position,
                            PhoneNumber = inhabitant.PhoneNumber
                        });
                }
            }
        }

        public void EditInhabitant(Inhabitant inhabitant)
        {
            if (checkInhabitantValid(inhabitant))
            {
                using (SqlConnection connection = new SqlConnection(ConnectionString))
                {
                    connection.Query(
                        @"UPDATE Inhabitants 
                            SET BuildingId = @BuildingId, Flat = @Flat, FirstName = @FirstName, LastName = @LastName,
                                MiddleName = @MiddleName, WhichSex = @WhichSex, BirthDate = @BirthDate, 
                                SettlementDate = @SettlementDate, EvictionDate = @EvictionDate, PassportId = @PassportId,
                                WorkPlace = @WorkPlace, Position = @Position
                                WHERE Id = @Id",
                        new
                        {
                            Id = inhabitant.Id,
                            BuildingId = inhabitant.BuildingId,
                            Flat = inhabitant.Flat,
                            FirstName = inhabitant.FirstName,
                            LastName = inhabitant.LastName,
                            MiddleName = inhabitant.MiddleName,
                            WhichSex = inhabitant.WhichSex,
                            BirthDate = inhabitant.BirthDate,
                            SettlementDate = inhabitant.SettlementDate,
                            EvictionDate = inhabitant.EvictionDate,
                            PassportId = inhabitant.PassportId,
                            WorkPlace = inhabitant.WorkPlace,
                            Position = inhabitant.Position,
                        });
                }
            }
        }

        public void DeleteInhabitant(int inhabitantId)
        {
            using (SqlConnection connection = new SqlConnection(ConnectionString))
            {
                connection.Query(@"DELETE FROM Inhabitants WHERE Id = @Id", new { Id = inhabitantId });
            }
        }

        public bool checkInhabitantValid(Inhabitant inhabitant)
        {
            if (inhabitant == null)
            {
                throw new Exception("Complete all fields");
            }

            if (inhabitant.FirstName == null || inhabitant.FirstName.Trim().Equals(""))
            {
                throw new Exception("Complete all fields");
            }

            if (inhabitant.LastName == null || inhabitant.LastName.Trim().Equals(""))
            {
                throw new Exception("Complete all fields");
            }

            if (inhabitant.PassportId == null || inhabitant.PassportId.Trim().Equals(""))
            {
                throw new Exception("Complete all fields");
            }

            if (inhabitant.Flat == null || inhabitant.Flat.Trim().Equals(""))
            {
                throw new Exception("Complete all fields");
            }

            return true;
        }
    }
}