﻿using System;
using System.Data.SqlClient;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Media.Media3D;
using Dapper;
using Source.Exceptions;
using Source.Interfaces;
using Source.Models;

namespace Source.Repositories
{
    public class UserRepository
    {
        public static User CurrentUser;
        private static readonly string ConnectionString =@"Data Source=SLAP\SQLEXPRESS;Initial Catalog=Source;Integrated Security=True";

        public bool Login(string username, string password)
        {
            if (username == null || username.Trim().Equals(""))
            {
                throw new AuthException("Enter username", true, false);
            }

            if (password == null || password.Trim().Equals(""))
            {
                throw new AuthException("Enter password", false, true);
            }

            using (SqlConnection connection = new SqlConnection(ConnectionString))
            {
                User user = connection.Query<User>(
                    @"SELECT * FROM Users 
                    WHERE Login = @Username and Password = @Password",
                    new
                    {
                        @Username = username,
                        @Password = password
                    }).FirstOrDefault();
                if (user == null)
                {
                    throw new AuthException("Wrong username or password", true, true);
                }
                else
                {
                    CurrentUser = user;
                    return true;
                }
            }
        }

        public bool Register(string username, string password, string confirmPassword)
        {
            if (isUsernamePermitted(username) & isPasswordReliable(password, confirmPassword))
            {
                using (SqlConnection connection = new SqlConnection(ConnectionString))
                {
                    if (!ifAccountExists(username))
                    {
                        connection.Query(
                            @"INSERT INTO Users (Login, Password) VALUES (@Username, @Password)",
                            new
                            {
                                @Username = username,
                                @Password = password,
                            });

                        CurrentUser = connection.Query<User>(
                            @"SELECT * FROM Users WHERE Login = @Username and Password = @Password",
                            new
                            {
                                @Username = username,
                                @Password = password
                            }).FirstOrDefault();
                        return true;
                    }
                    else
                    {
                        throw new AuthException("An account with the same name already exists", true, false);
                    }
                }
            }

            return false;
        }

        private bool isUsernamePermitted(string username)
        {
            var usernameRegex = new Regex(@"^[a-zA-Z0-9]*$");
            var lengthRegex = new Regex(@"^.{3,15}$");
            if (username == null)
            {
                throw new AuthException("Come up with a username",
                    true, false);
            }

            if (!usernameRegex.IsMatch(username))
            {
                throw new AuthException("Username should contain only latin letters and numbers without spaces",
                    true, false);
            }

            if (!lengthRegex.IsMatch(username))
            {
                throw new AuthException("Username length must be between 3 and 30 characters",
                    true, false);
            }

            return true;
        }

        private bool isPasswordReliable(string password, string confirmPassword)
        {
            /*if (password.Contains(" "))
            {
                throw new AuthException("Password should no contain spaces", false, true);
            }

            if (password.Length < 8)
            {
                throw new AuthException("Password should contain at least 8 characters", false, true);
            }

            int letters = Regex.Matches(password, @"[a-zA-Z]").Count;
            if (letters < 2)
            {
                throw new AuthException("Password should contain at least 2 letters", false, true);
            }

            if (!password.Equals(confirmPassword))
            {
                throw new AuthException("Passwords do not match", false, true);
            }

            return true;*/
            var hasNumber = new Regex(@"[0-9]+");
            var hasUpperChar = new Regex(@"[A-Z]+");
            var hasMiniMaxChars = new Regex(@".{8,15}");
            var hasLowerChar = new Regex(@"[a-z]+");
            if (password == null)
            {
                throw new AuthException("Come up with a password",
                    true, false);
            }

            if (!hasMiniMaxChars.IsMatch(password))
            {
                throw new AuthException("Password should not be lesser than 8 or greater than 15 characters.",
                    false, true);
            }

            if (!hasNumber.IsMatch(password))
            {
                throw new AuthException("Password should contain at least one numeric value.",
                    false, true);
            }

            if (!hasLowerChar.IsMatch(password))
            {
                throw new AuthException("Password should contain at least one lower case letter.",
                    false, true);
            }

            if (!hasUpperChar.IsMatch(password))
            {
                throw new AuthException("Password should contain at least one upper case letter.",
                    false, true);
            }

            if (!password.Equals(confirmPassword))
            {
                throw new AuthException("Passwords do not match", false, true);
            }

            return true;
        }

        public bool ifAccountExists(string login)
        {
            using (SqlConnection connection = new SqlConnection(ConnectionString))
            {
                string loginExists = connection.Query<string>(
                    @"SELECT Login FROM Users WHERE Login = @Login",
                    new
                    {
                        @Login = login
                    }).FirstOrDefault();
                if (loginExists == null)
                    return false;
                else
                    return true;
            }
        }
        public static bool ifUserAuthorized()
        {
            if (CurrentUser != null)
            {

                using (SqlConnection connection = new SqlConnection(ConnectionString))
                {
                    User user = connection.Query<User>(
                        @"SELECT Login FROM Users WHERE Login = @Login and Id = @Id",
                        new
                        {
                            @Login = CurrentUser.Login,
                            @Id = CurrentUser.Id
                        }).FirstOrDefault();
                    if (user == null)
                        return false;
                    return true;
                }
            }

            return false;
        }
    }
}