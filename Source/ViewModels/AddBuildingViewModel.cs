﻿using System;
using System.Windows.Input;
using Source.Delegates;
using Source.Models;
using Source.Repositories;

namespace Source.ViewModels
{
    public class AddBuildingViewModel : BaseVM
    {
        enum Mode
        {
            Create,
            Edit
        }

        private Mode currentMode;
        public Action CloseAction { get; set; }
        private string title { get; set; }
        private string name { get; set; }
        private string address { get; set; }
        private string errorMessage { get; set; }
        private string imagePath { get; set; }
        private string buttonText { get; set; }
        private int complexId { get; set; }
        private int buildingId { get; set; }
        private BuildingRepository buildingRepository = new BuildingRepository();

        public string Title
        {
            get { return title; }
            set
            {
                title = value;
                OnPropertyChanged();
            }
        }

        public string Name
        {
            get { return name; }
            set
            {
                name = value;
                OnPropertyChanged();
            }
        }

        public string Address
        {
            get { return address; }
            set
            {
                address = value;
                OnPropertyChanged();
            }
        }

        public string ErrorMessage
        {
            get { return errorMessage; }
            set
            {
                errorMessage = value;
                OnPropertyChanged();
            }
        }

        public string ImagePath
        {
            get { return imagePath; }
            set
            {
                imagePath = value;
                OnPropertyChanged();
            }
        }

        public string ButtonText
        {
            get { return buttonText; }
            set
            {
                buttonText = value;
                OnPropertyChanged();
            }
        }

        public AddBuildingViewModel()
        {
            currentMode = Mode.Create;
        }

        public AddBuildingViewModel(int complexId)
        {
            currentMode = Mode.Create;
            this.complexId = complexId;
            Name = $"Building #{buildingRepository.GetBuildingsCount(complexId) + 1}";
            ImagePath = "/Resources/Icons/add_icon.png";
            ButtonText = "Add building";
            Title = "New building";
        }

        public AddBuildingViewModel(Building building)
        {
            currentMode = Mode.Edit;
            Name = building.Name;
            Address = building.Address;
            complexId = building.ComplexId;
            buildingId = building.Id;
            ImagePath = "/Resources/Icons/done_icon.png";
            ButtonText = "Save changes";
            Title = "Edit building";
        }

        public ICommand AddBuilding
        {
            get
            {
                return new DelegateCommand<object>((obj) =>
                {
                    if (currentMode == Mode.Create)
                    {
                        try
                        {
                            Building building = new Building(Name, complexId, Address);
                            buildingRepository.AddBuilding(building);
                            CloseAction();
                        }
                        catch (Exception e)
                        {
                            ErrorMessage = e.Message;
                        }
                    }
                    else
                    {
                        try
                        {
                            Building building = new Building(buildingId, Name, complexId, Address);
                            buildingRepository.EditBuilding(building);
                            CloseAction();
                        }
                        catch (Exception e)
                        {
                            ErrorMessage = e.Message;
                        }
                    }
                });
            }
        }
    }
}