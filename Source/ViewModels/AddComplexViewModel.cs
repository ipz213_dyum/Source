﻿using System;
using System.Runtime.InteropServices.ComTypes;
using System.Windows.Input;
using Source.Delegates;
using Source.Models;
using Source.Repositories;
using Source.Views;

namespace Source.ViewModels
{
    public class AddComplexViewModel : BaseVM
    {
        enum Mode
        {
            Create,
            Edit
        }

        private Mode currentMode;
        private string title { get; set; }
        private string name { get; set; }
        private string city { get; set; }
        private string errorMessage { get; set; }
        private int complexId { get; set; }
        private string imagePath { get; set; }
        private string buttonText { get; set; }
        public Action CloseAction { get; set; }
        private ComplexRepository complexRepository;

        public string Name
        {
            get { return name; }
            set
            {
                name = value;
                OnPropertyChanged();
            }
        }

        public string City
        {
            get { return city; }
            set
            {
                city = value;
                OnPropertyChanged();
            }
        }

        public string ErrorMessage
        {
            get { return errorMessage; }
            set
            {
                errorMessage = value;
                OnPropertyChanged();
            }
        }

        public string Title
        {
            get { return title; }
            set
            {
                title = value;
                OnPropertyChanged();
            }
        }

        public int ComplexId
        {
            get { return complexId; }
            set
            {
                complexId = value;
                OnPropertyChanged();
            }
        }

        public string ImagePath
        {
            get { return imagePath; }
            set
            {
                imagePath = value;
                OnPropertyChanged();
            }
        }

        public string ButtonText
        {
            get { return buttonText; }
            set
            {
                buttonText = value;
                OnPropertyChanged();
            }
        }

        public AddComplexViewModel()
        {
            currentMode = Mode.Create;
            complexRepository = new ComplexRepository();
            Name = $"Complex #{complexRepository.GetComplexesCount() + 1}";
            ImagePath = "/Resources/Icons/add_icon.png";
            ButtonText = "Add complex";
            Title = "New complex";
        }

        public AddComplexViewModel(Complex complex)
        {
            currentMode = Mode.Edit;
            complexRepository = new ComplexRepository();
            Name = complex.Name;
            City = complex.City;
            ComplexId = complex.Id;
            ImagePath = "/Resources/Icons/done_icon.png";
            ButtonText = "Save changes";
            Title = "Edit complex";

        }

        public ICommand AddComplex
        {
            get
            {
                return new DelegateCommand<object>((obj) =>
                {
                    if (currentMode == Mode.Create)
                    {
                        try
                        {
                            Complex complex = new Complex(City, Name);
                            complexRepository.AddComplex(complex);
                            CloseAction();
                        }
                        catch (Exception e)
                        {
                            ErrorMessage = e.Message;
                        }
                    }
                    else
                    {
                        try
                        {
                            Complex complex = new Complex(ComplexId, UserRepository.CurrentUser.Id,City, Name);
                            complexRepository.EditComplex(complex);
                            CloseAction();
                        }
                        catch (Exception e)
                        {
                            ErrorMessage = e.Message;
                        }
                    }
                });
            }
        }
    }
}