﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Linq;
using System.Windows.Input;
using Source.Delegates;
using Source.Models;
using Source.Repositories;
using Source.Views;

namespace Source.ViewModels
{
    public class ComplexViewModel : BaseVM
    {
        private ComplexRepository complexRepository = new ComplexRepository();
        private BuildingRepository buildingRepository = new BuildingRepository();
        private InhabitantRepository inhabitantRepository = new InhabitantRepository();
        public Action CloseAction { get; set; }
        private Complex currentComplex { get; set; }
        private Building currentBuilding { get; set; }
        private string inhabitantsTextBlock { get; set; }
        private ObservableCollection<Building> buildings { get; set; }
        private ObservableCollection<Inhabitant> inhabitants { get; set; }
        private Building selectedBuilding { get; set; }
        private int scrollBarsHeight { get; set; }
        private int scrollBarsHeight2 { get; set; }
        private string search { get; set; }

        public string Search
        {
            get { return search; }
            set
            {
                search = value;
                FilterInhabitants();
                OnPropertyChanged();
            }
        }

        public int ScrollBarsHeight
        {
            get { return scrollBarsHeight; }
            set
            {
                scrollBarsHeight = value;
                OnPropertyChanged();
            }
        }

        public int ScrollBarsHeight2
        {
            get { return scrollBarsHeight2; }
            set
            {
                scrollBarsHeight2 = value;
                OnPropertyChanged();
            }
        }

        public ObservableCollection<Building> Buildings
        {
            get { return buildings; }
            set
            {
                buildings = value;
                OnPropertyChanged();
            }
        }

        public ObservableCollection<Inhabitant> Inhabitants
        {
            get { return inhabitants; }
            set
            {
                inhabitants = value;
                OnPropertyChanged();
            }
        }

        public Complex CurrentComplex
        {
            get { return currentComplex; }
            set
            {
                currentComplex = value;
                OnPropertyChanged();
            }
        }

        public Building CurrentBuilding
        {
            get { return currentBuilding; }
            set
            {
                currentBuilding = value;
                InhabitantsTextBlock = currentBuilding?.Name;
                OnPropertyChanged();
            }
        }

        public string InhabitantsTextBlock
        {
            get { return currentBuilding != null ? currentBuilding.Name : "Complex"; }
            set
            {
                inhabitantsTextBlock = value;
                OnPropertyChanged();
            }
        }

        public ComplexViewModel(Complex complex)
        {
            CurrentComplex = complex;
            Buildings = new ObservableCollection<Building>(buildingRepository.GetBuildings(CurrentComplex.Id));
            Inhabitants =
                new ObservableCollection<Inhabitant>(complexRepository.GetComplexInhabitants(CurrentComplex.Id));
            CurrentComplex.City = Inhabitants.Count.ToString();
        }

        public ComplexViewModel()
        {
        }

        public ICommand EditComplex
        {
            get
            {
                return new DelegateCommand<object>((obj) =>
                {
                    AddComplexWindow addComplexWindow = new AddComplexWindow();
                    AddComplexViewModel addComplexViewModel = new AddComplexViewModel(currentComplex);
                    addComplexWindow.DataContext = addComplexViewModel;
                    addComplexWindow.initializeOnClosing();
                    addComplexWindow.Open();
                });
            }
        }

        public ICommand AddBuilding
        {
            get
            {
                return new DelegateCommand<object>((obj) =>
                {
                    NewBuilding newBuilding = new NewBuilding();
                    AddBuildingViewModel addBuildingViewModel = new AddBuildingViewModel(currentComplex.Id);
                    newBuilding.DataContext = addBuildingViewModel;
                    newBuilding.initializeOnClosing();
                    newBuilding.Open();
                });
            }
        }

        public ICommand RemoveComplex
        {
            get
            {
                return new DelegateCommand<object>((obj) =>
                {
                    ConfirmationWindow confirmationWindow = new ConfirmationWindow();
                    ConfirmationViewModel confirmationViewModel = new ConfirmationViewModel("Complex deleting",
                        "Are you sure all data bounded with the complex will be deleted?", new Action(Remove));
                    confirmationWindow.DataContext = confirmationViewModel;
                    confirmationWindow.initializeOnClosing();
                    confirmationWindow.Open();
                });
            }
        }

        public void Remove()
        {
            complexRepository.DeleteComplex(CurrentComplex.Id);
            CloseAction();
        }


        public void UpdateData()
        {
            CurrentComplex = complexRepository.GetComplex(currentComplex.Id);
            Buildings.Clear();
            foreach (var building in buildingRepository.GetBuildings(currentComplex.Id))
            {
                Buildings.Add(building);
            }


            Inhabitants.Clear();
            if (CurrentBuilding == null && (Search == null || Search.Trim().Equals("")))
            {
                foreach (var inhabitant in complexRepository.GetComplexInhabitants(currentComplex.Id))
                {
                    Inhabitants.Add(inhabitant);
                }
            }
            else if (Search != null && !Search.Trim().Equals(""))
            {
                FilterInhabitants();
            }
            else
            {
                foreach (var inhabitant in inhabitantRepository.GetBuildingInhabitants(CurrentBuilding.Id))
                {
                    Inhabitants.Add(inhabitant);
                }
            }
        }

        public ICommand EditBuilding
        {
            get
            {
                return new DelegateCommand<Building>((obj) =>
                {
                    Building building = (Building)obj;
                    NewBuilding newBuilding = new NewBuilding();
                    AddBuildingViewModel addBuildingViewModel = new AddBuildingViewModel(building);
                    newBuilding.DataContext = addBuildingViewModel;
                    newBuilding.initializeOnClosing();
                    newBuilding.Open();
                });
            }
        }

        public ICommand DeleteBuilding
        {
            get
            {
                return new DelegateCommand<Building>((obj) =>
                {
                    selectedBuilding = (Building)obj;
                    ConfirmationWindow confirmationWindow = new ConfirmationWindow();
                    ConfirmationViewModel confirmationViewModel = new ConfirmationViewModel("Building deleting",
                        "Are you sure all data bounded with this building will be deleted?",
                        new Action(RemoveBuilding));
                    confirmationWindow.DataContext = confirmationViewModel;
                    confirmationWindow.initializeOnClosing();
                    confirmationWindow.Open();
                });
            }
        }

        public ICommand FilterBuilding
        {
            get
            {
                return new DelegateCommand<Building>((obj) =>
                {
                    CurrentBuilding = (Building)obj;
                    Inhabitants.Clear();
                    foreach (var inhabitant in inhabitantRepository.GetBuildingInhabitants(CurrentBuilding.Id))
                    {
                        Inhabitants.Add(inhabitant);
                    }
                });
            }
        }

        public ICommand OpenInhabitant
        {
            get
            {
                return new DelegateCommand<Inhabitant>((obj) =>
                {
                    InhabitantWindow inhabitantWindow = new InhabitantWindow();
                    InhabitantViewModel inhabitantViewModel = new InhabitantViewModel(CurrentComplex, (Inhabitant)obj);
                    inhabitantWindow.DataContext = inhabitantViewModel;
                    inhabitantWindow.initializeOnClosing();
                    inhabitantWindow.Open();
                });
            }
        }

        public ICommand AddInhabitant
        {
            get
            {
                return new DelegateCommand<object>((obj) =>
                {
                    if (Buildings.Count != 0)
                    {
                        InhabitantWindow inhabitantWindow = new InhabitantWindow();
                        InhabitantViewModel inhabitantViewModel = new InhabitantViewModel(CurrentComplex);
                        inhabitantWindow.DataContext = inhabitantViewModel;
                        inhabitantWindow.initializeOnClosing();
                        inhabitantWindow.Open();
                    }
                });
            }
        }

        public ICommand ShowAllInhabitants
        {
            get
            {
                return new DelegateCommand<object>((obj) =>
                {
                    CurrentBuilding = null;
                    Inhabitants.Clear();
                    foreach (var inhabitant in complexRepository.GetComplexInhabitants(CurrentComplex.Id))
                    {
                        Inhabitants.Add(inhabitant);
                    }
                });
            }
        }

        public void RemoveBuilding()
        {
            buildingRepository.DeleteBuilding(selectedBuilding.Id);
        }

        private void FilterInhabitants()
        {
            List<Inhabitant> filterList;
            if (CurrentBuilding == null)
            {
                filterList = complexRepository.GetComplexInhabitants(currentComplex.Id);
            }
            else
            {
                filterList = inhabitantRepository.GetBuildingInhabitants(CurrentBuilding.Id);
            }

            string searchRequest = Search.ToLower();
            if (!search.Trim().Equals(""))
            {
                Inhabitants.Clear();

                foreach (var inhabitant in filterList)
                {
                    if (inhabitant.FirstName.ToLower().Contains(searchRequest))
                    {
                        Inhabitants.Add(inhabitant);
                    }
                    else if (inhabitant.LastName.ToLower().Contains(searchRequest))
                    {
                        Inhabitants.Add(inhabitant);
                    }

                    else if (inhabitant.SexString.ToLower().Equals(searchRequest))
                    {
                        Inhabitants.Add(inhabitant);
                    }
                    else if (inhabitant.Age.ToString().ToLower().Equals(searchRequest))
                    {
                        Inhabitants.Add(inhabitant);
                    }
                    else if (inhabitant.Flat.ToLower().Equals(searchRequest))
                    {
                        Inhabitants.Add(inhabitant);
                    }
                    else if (inhabitant.PassportId.ToLower().Contains(searchRequest))
                    {
                        Inhabitants.Add(inhabitant);
                    }
                    else if (inhabitant.MiddleName != null)
                    {
                        if (inhabitant.MiddleName.ToLower().Contains(searchRequest))
                        {
                            Inhabitants.Add(inhabitant);
                        }
                    }
                    else if (inhabitant.PhoneNumber != null)
                    {
                        if (inhabitant.PhoneNumber.Contains(searchRequest))
                        {
                            Inhabitants.Add(inhabitant);
                        }
                    }
                    else if (inhabitant.WorkPlace != null)
                    {
                        if (inhabitant.WorkPlace.ToLower().Contains(searchRequest))
                        {
                            Inhabitants.Add(inhabitant);
                        }
                    }
                    else if (inhabitant.Position != null)
                    {
                        if (inhabitant.Position.ToLower().Contains(searchRequest))
                        {
                            Inhabitants.Add(inhabitant);
                        }
                    }
                }
            }
            else
            {
                Inhabitants.Clear();
                foreach (var inhabitant in complexRepository.GetComplexInhabitants(currentComplex.Id))
                {
                    Inhabitants.Add(inhabitant);
                }
            }
        }
    }
}