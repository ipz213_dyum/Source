﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;
using System.Windows.Input;
using Source.Delegates;
using Source.Exceptions;
using Source.Interfaces;
using Source.Models;
using Source.Repositories;
using Source.Views;

namespace Source.ViewModels
{
    public class ComplexesViewModel : BaseVM
    {
        private ComplexRepository complexRepository;
        public ObservableCollection<Complex> Complexes { get; set; }
        private bool isAuthorized { get; set; } = false;
        private string username { get; set; }
        private int scrollBarsHeight { get; set; }
        public string Username
        {
            get { return username; }
            set
            {
                username = value;
                OnPropertyChanged();
            }
        }
        public bool IsAuthorized
        {
            get { return isAuthorized; }
            set
            {
                isAuthorized = value;
                OnPropertyChanged();
            }
        }
        public int ScrollBarsHeight
        {
            get { return scrollBarsHeight; }
            set
            {
                scrollBarsHeight = value;
                OnPropertyChanged();
            }
        }
        public ComplexesViewModel()
        {
            complexRepository = new ComplexRepository();
            Complexes = new ObservableCollection<Complex>();
            
        }

        public ICommand OpenLoginWindow
        {
            get
            {
                return new DelegateCommand<object>((obj) =>
                {
                    LoginWindow loginWindow = new LoginWindow();
                    loginWindow.Open();
                });
            }
        }
        public ICommand AddComplex
        {
            get
            {
                return new DelegateCommand<object>((obj) =>
                {
                    AddComplexWindow addComplexWindow = new AddComplexWindow();
                    addComplexWindow.Open();
                });
            }
        }
        public ICommand LogOut
        {
            get
            {
                return new DelegateCommand<object>((obj) =>
                {
                    UserRepository.CurrentUser = null;
                    IsAuthorized = UserRepository.ifUserAuthorized();
                    LoginWindow loginWindow = new LoginWindow();
                    loginWindow.Open();
                });
            }
        }
        public ICommand OpenComplex
        {
            get
            {
                return new DelegateCommand<Complex>((item) =>
                {
                    Complex complex = (Complex)item;
                    ComplexViewWindow complexViewWindow = new ComplexViewWindow();
                    ComplexViewModel complexViewModel = new ComplexViewModel(complex);
                    complexViewWindow.DataContext= complexViewModel;
                    complexViewWindow.initializeOnClosing();
                    complexViewWindow.Open();
                });
            }
        }

        
        public void CheckAuth()
        {
            IsAuthorized = UserRepository.ifUserAuthorized();
            if (IsAuthorized)
            {
                Complexes.Clear();
                foreach (var complex in complexRepository.GetComplexes())
                    Complexes.Add(complex);
                Username = UserRepository.CurrentUser.Login;
            }
        }
        
    }
}