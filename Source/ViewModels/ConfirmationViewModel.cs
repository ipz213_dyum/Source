﻿using System;
using System.Windows.Input;
using Source.Delegates;
using Source.Interfaces;
using Source.Repositories;

namespace Source.ViewModels
{
    public class ConfirmationViewModel:BaseVM
    {
        public Action CloseAction { get; set; }
        private Action WhenOk { get; set; }
        private string title { get; set; }
        private string message { get; set; }

        public string Title
        {
            get { return title; }
            set
            {
                title = value;
                OnPropertyChanged();
            }
        }
        public string Message
        {
            get { return message; }
            set
            {
                message = value;
                OnPropertyChanged();
            }
        }

        public ConfirmationViewModel()
        {
            
        }
        public ConfirmationViewModel(string title, string message, Action whenOk)
        {
            Title = title;
            Message = message;
            WhenOk = whenOk;
        }

        public ICommand Ok
        {
            get
            {
                return new DelegateCommand<object>((obj) =>
                {
                    WhenOk();
                    CloseAction();
                });
            }
        }
        public ICommand Cancel
        {
            get
            {
                return new DelegateCommand<object>((obj) =>
                {
                    CloseAction();
                });
            }
        }
    }
}