﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.IO;
using System.Runtime.Remoting.Channels;
using System.Windows.Controls.Primitives;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media.Animation;
using Source.Delegates;
using Source.Models;
using Source.Repositories;
using Source.Views;

namespace Source.ViewModels
{
    public class InhabitantViewModel : BaseVM
    {
        enum Mode
        {
            Create,
            Edit
        }

        private Mode currentMode;
        private string title;
        private string errorMessage;
        public Action CloseAction { get; set; }
        private InhabitantRepository inhabitantRepository = new InhabitantRepository();
        private BuildingRepository buildingRepository = new BuildingRepository();
        private Complex currentComplex { get; set; }
        private Building curentBuilding { get; set; }
        private Inhabitant currentInhabitant { get; set; }
        private string deleteButtonText { get; set; }
        private List<Building> buildings { get; set; }
        private int selectedSexIndex { get; set; }
        private int selectedBuildingIndex { get; set; }
        private DateTime birthDate { get; set; }
        private DateTime settlementDate { get; set; }
        private DateTime? evictionDate { get; set; }

        public string Title
        {
            get { return title; }
            set
            {
                title = value;
                OnPropertyChanged();
            }
        }

        public string ErrorMessage
        {
            get { return errorMessage; }
            set
            {
                errorMessage = value;
                OnPropertyChanged();
            }
        }

        public Complex CurrentComplex
        {
            get { return currentComplex; }
            set
            {
                currentComplex = value;
                OnPropertyChanged();
            }
        }

        public Building CurrentBuilding
        {
            get { return curentBuilding; }
            set
            {
                curentBuilding = value;
                OnPropertyChanged();
            }
        }

        public Inhabitant CurrentInhabitant
        {
            get { return currentInhabitant; }
            set
            {
                currentInhabitant = value;
                OnPropertyChanged();
            }
        }

        public string DeleteButtonText
        {
            get { return deleteButtonText; }
            set
            {
                deleteButtonText = value;
                OnPropertyChanged();
            }
        }

        public DateTime BirthDate
        {
            get { return birthDate; }
            set
            {
                birthDate = value;
                OnPropertyChanged();
            }
        }

        public DateTime SettlementDate
        {
            get { return settlementDate; }
            set
            {
                settlementDate = value;
                OnPropertyChanged();
            }
        }

        public DateTime? EvictionDate
        {
            get { return evictionDate; }
            set
            {
                evictionDate = value;
                OnPropertyChanged();
            }
        }

        public string[] WhichSexList
        {
            get
            {
                string[] whichSex =
                {
                    "Not known",
                    "Male",
                    "Female",
                    "Other"
                };
                return whichSex;
            }
        }

        public List<Building> Buildings
        {
            get { return buildings; }
            set
            {
                buildings = value;
                OnPropertyChanged();
            }
        }

        public int SelectedSexIndex
        {
            get { return selectedSexIndex; }
            set
            {
                selectedSexIndex = value;
                OnPropertyChanged();
            }
        }

        public int SelectedBuildingIndex
        {
            get { return selectedBuildingIndex; }
            set
            {
                selectedBuildingIndex = value;
                OnPropertyChanged();
            }
        }

        public InhabitantViewModel()
        {
        }

        public InhabitantViewModel(Complex complex)
        {
            currentMode = Mode.Create;
            CurrentComplex = complex;
            DeleteButtonText = "Cancel";
            buildings = new List<Building>();
            buildings = buildingRepository.GetBuildings(CurrentComplex.Id);
            Title = "Add Inhabitant";
            CurrentInhabitant = new Inhabitant();
            SettlementDate = DateTime.Now;
            BirthDate = new DateTime(2000, 1, 1);
        }

        public InhabitantViewModel(Complex complex, Inhabitant inhabitant)
        {
            currentMode = Mode.Edit;
            CurrentComplex = complex;
            CurrentInhabitant = inhabitant;
            DeleteButtonText = "Delete";
            Buildings = buildingRepository.GetBuildings(CurrentComplex.Id);
            Title = $"{inhabitant.FirstName} {inhabitant.LastName}";
            SelectedSexIndex = (int)inhabitant.WhichSex;
            BirthDate = CurrentInhabitant.BirthDate;
            SettlementDate = CurrentInhabitant.SettlementDate;
            EvictionDate = CurrentInhabitant.EvictionDate;
        }

        public ICommand DeleteInhabitant
        {
            get
            {
                return new DelegateCommand<object>((obj) =>
                {
                    if (currentMode == Mode.Create)
                    {
                        CloseAction();
                    }
                    else
                    {
                        ConfirmationWindow confirmationWindow = new ConfirmationWindow();
                        ConfirmationViewModel confirmationViewModel = new ConfirmationViewModel("Inhabitant Deleting",
                            "Are you sure you want to delete the information about the resident?", new Action(Remove));
                        confirmationWindow.DataContext = confirmationViewModel;
                        confirmationWindow.initializeOnClosing();
                        confirmationWindow.Open();
                    }
                });
            }
        }

        public ICommand SaveInhabitant
        {
            get
            {
                return new DelegateCommand<object>((obj) =>
                {
                    CurrentInhabitant.WhichSex = (Inhabitant.Sex)SelectedSexIndex;
                    CurrentInhabitant.BuildingId = Buildings[SelectedBuildingIndex].Id;
                    CurrentInhabitant.BirthDate = BirthDate;
                    CurrentInhabitant.SettlementDate = SettlementDate;
                    CurrentInhabitant.EvictionDate = EvictionDate;
                    try
                    {
                        if (currentMode == Mode.Create)
                        {
                            inhabitantRepository.AddInhabitant(CurrentInhabitant);
                            CloseAction();
                        }
                        else
                        {
                            inhabitantRepository.EditInhabitant(CurrentInhabitant);
                            CloseAction();
                        }
                    }
                    catch (Exception e)
                    {
                        ErrorMessage = e.Message;
                    }
                });
            }
        }

        public void Remove()
        {
            inhabitantRepository.DeleteInhabitant(currentInhabitant.Id);
            CloseAction();
        }

        
    }
}