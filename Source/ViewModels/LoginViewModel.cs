﻿using System;
using System.Security;
using System.Windows.Input;
using System.Windows.Media.Animation;
using Source.Delegates;
using Source.Exceptions;
using Source.Models;
using Source.Repositories;

namespace Source.ViewModels
{
    public class LoginViewModel : BaseVM
    {
        private UserRepository userRepository;
        private string userName;
        public string Password { private get; set; }
        public string ConfirmPassword { private get; set; }
        private string errorMessage;
        private bool usernameError;
        private bool passwordError;
        private bool isRegisterMode;
        private string primaryActionText = "Log in";
        private string secondaryActionText = "Register";
        private static string RegisterText = "Have no account?";
        private static string LoginText = "Already have an account?";
        private string switchModeText = RegisterText;
        public Action CloseAction  { get; set;}
        public string UserName
        {
            get { return userName; }
            set
            {
                if (!string.Equals(userName, value))
                {
                    userName = value;
                    OnPropertyChanged();
                }
            }
        }

        public string ErrorMessage
        {
            get { return errorMessage; }
            set
            {
                if (!string.Equals(errorMessage, value))
                {
                    errorMessage = value;
                    OnPropertyChanged();
                }
            }
        }
        public bool IsRegisterMode
        {
            get { return isRegisterMode; }
            set
            {
                isRegisterMode = value;
                OnPropertyChanged();
            }
        }

        public string PrimaryActionText
        {
            get { return primaryActionText; }
            set
            {
                primaryActionText = value;
                OnPropertyChanged();
            }
        }
        public string SecondaryActionText
        {
            get { return secondaryActionText; }
            set
            {
                secondaryActionText = value;
                OnPropertyChanged();
            }
        }
        public string SwitchModeText
        {
            get { return switchModeText; }
            set
            {
                switchModeText = value;
                OnPropertyChanged();
            }
        }
        public bool UsernameError
        {
            get { return usernameError; }
            set
            {
                usernameError = value;
                OnPropertyChanged();
            }
        }
        public bool PasswordError
        {
            get { return passwordError; }
            set
            {
                passwordError = value;
                OnPropertyChanged();
            }
        }
        public ICommand PrimaryAction
        {
            get
            {
                return new DelegateCommand<object>((obj) =>
                {
                    try
                    {
                        if (isRegisterMode)
                        {
                            if (userRepository.Register(userName, Password, ConfirmPassword))
                            {
                                CloseAction();
                            }
                        }
                        else
                        {
                            if (userRepository.Login(userName, Password))
                            {
                                CloseAction();
                            } 
                        }
                    }
                    catch (AuthException e)
                    {
                        PasswordError = e.PasswordIncorrect;
                        UsernameError = e.UsernameIncorrect;
                        ErrorMessage = e.Message;
                    }
                });
            }
        }

        public ICommand SecondaryAction
        {
            get
            {
                return new DelegateCommand<object>((obj) =>
                {
                    IsRegisterMode = !IsRegisterMode;
                    (PrimaryActionText, SecondaryActionText) = (SecondaryActionText, PrimaryActionText);
                    SwitchModeText = SwitchModeText == RegisterText ? LoginText : RegisterText;
                    ErrorMessage = "";
                    UsernameError = false;
                    PasswordError = false;
                });
            }
        }
        public LoginViewModel()
        {
            userRepository = new UserRepository();
        }
    }
}