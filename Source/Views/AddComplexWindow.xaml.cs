﻿using System;
using System.ComponentModel;
using System.Windows;
using Source.Interfaces;
using Source.ViewModels;

namespace Source.Views
{
    public partial class AddComplexWindow : Window, IView
    {
        public AddComplexWindow()
        {
            InitializeComponent();
            if (((AddComplexViewModel)(this.DataContext)).CloseAction == null)
                ((AddComplexViewModel)(this.DataContext)).CloseAction = new Action(this.Close);
        }

        public void initializeOnClosing()
        {
            if (((AddComplexViewModel)(this.DataContext)).CloseAction == null)
                ((AddComplexViewModel)(this.DataContext)).CloseAction = new Action(this.Close);
        }
        protected override void OnClosing(CancelEventArgs e)
        {
            this.Visibility = Visibility.Collapsed;
            e.Cancel = true;
        }

        public bool? Open()
        {
            return this.ShowDialog();
        }
    }
}