﻿using System;
using System.Windows;
using Source.Interfaces;
using Source.Models;
using Source.Repositories;
using Source.ViewModels;

namespace Source.Views
{
    public partial class ComplexViewWindow : Window
    {
        public ComplexViewWindow()
        {
            InitializeComponent();
        }

        public void initializeOnClosing()
        {
            if (((ComplexViewModel)(this.DataContext)).CloseAction == null)
                ((ComplexViewModel)(this.DataContext)).CloseAction = new Action(this.Close);
        }

        protected override void OnActivated(EventArgs e)
        {
            ((ComplexViewModel)this.DataContext).UpdateData();
        }

        protected override void OnRenderSizeChanged(SizeChangedInfo sizeInfo)
        {
            ((ComplexViewModel)(this.DataContext)).ScrollBarsHeight = (int)(ActualHeight-235);
            ((ComplexViewModel)(this.DataContext)).ScrollBarsHeight2 = (int)(ActualHeight-300);
        }

        public void Open()
        {
           this.Show();
        }
    }
}