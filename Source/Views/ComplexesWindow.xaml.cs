﻿using System;
using System.Windows;
using System.Windows.Input;
using Source.ViewModels;

namespace Source.Views
{
    public partial class ComplexesWindow : Window
    {
        /*
        private bool shown;
        */
        public ComplexesWindow()
        {
            InitializeComponent();
        }

        protected override void OnActivated(EventArgs e)
        {
            base.OnActivated(e);
            ((ComplexesViewModel)this.DataContext)?.CheckAuth();

        }
        protected override void OnRenderSizeChanged(SizeChangedInfo sizeInfo)
        {
            ((ComplexesViewModel)(this.DataContext)).ScrollBarsHeight = (int)(ActualHeight-220);
        }
    }
}