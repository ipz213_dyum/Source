﻿using System;
using System.ComponentModel;
using System.Windows;
using Source.Interfaces;
using Source.ViewModels;

namespace Source.Views
{
    public partial class ConfirmationWindow : Window, IView
    {
        public ConfirmationWindow()
        {
            InitializeComponent();
            initializeOnClosing();
        }
        public void initializeOnClosing()
        {
            
            if (((ConfirmationViewModel)(this.DataContext)).CloseAction == null)
                ((ConfirmationViewModel)(this.DataContext)).CloseAction = new Action(this.Close);
        }
        

        public bool? Open()
        {
            return this.ShowDialog();
        }
    }
}