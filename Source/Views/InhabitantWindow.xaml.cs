﻿using System;
using System.Windows;
using Source.Interfaces;
using Source.ViewModels;

namespace Source.Views
{
    public partial class InhabitantWindow : Window
    {
        public InhabitantWindow()
        {
            InitializeComponent();
            initializeOnClosing();
        }

        public void Open()
        {
             this.Show();
        }

        public void initializeOnClosing()
        {
            if (((InhabitantViewModel)(this.DataContext)).CloseAction == null)
                ((InhabitantViewModel)(this.DataContext)).CloseAction = new Action(this.Close);
        }
    }
}