﻿using System;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using Source.Interfaces;
using Source.ViewModels;

namespace Source.Views
{
    public partial class LoginWindow : Window, IView
    {
        public LoginWindow()
        {
            InitializeComponent();
            initializeOnClosing();
        }

        private void OnPasswordChanged(object sender, RoutedEventArgs e)
        {
            if (this.DataContext != null)
            {
                ((LoginViewModel)(this.DataContext)).Password = ((PasswordBox)sender).Password;
            }
        }

        private void OnConfirmPasswordChanged(object sender, RoutedEventArgs e)
        {
            if (this.DataContext != null)
            {
                ((LoginViewModel)(this.DataContext)).ConfirmPassword = ((PasswordBox)sender).Password;
            }
        }

        protected override void OnClosing(CancelEventArgs e)
        {
            this.Visibility = Visibility.Collapsed;
            e.Cancel = true;
        }

        public bool? Open()
        {
            return this.ShowDialog();
        }

        public void initializeOnClosing()
        {
            if (((LoginViewModel)(this.DataContext)).CloseAction == null)
                ((LoginViewModel)(this.DataContext)).CloseAction = new Action(this.Close);
        }
    }
}