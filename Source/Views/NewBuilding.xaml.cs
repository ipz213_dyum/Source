﻿using System;
using System.Windows;
using Source.Interfaces;
using Source.ViewModels;

namespace Source.Views
{
    public partial class NewBuilding : Window, IView
    {
        public NewBuilding()
        {
            InitializeComponent();
            initializeOnClosing();
        }
        

        public bool? Open()
        {
            return this.ShowDialog();
        }

        public void initializeOnClosing()
        {
            if (((AddBuildingViewModel)(this.DataContext)).CloseAction == null)
                ((AddBuildingViewModel)(this.DataContext)).CloseAction = new Action(this.Close);
        }
    }
}